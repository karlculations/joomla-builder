
Packages a Joomla extension (a component, module, package, plugin or template).

Author: Karl Goddard
Version: 2020-01-01


Usage:
- build-extension.sh
- build-extension.sh `source`
- build-extension.sh `source` `log`

**The log parameter is for debugging only.**


Source
======

If no source folder is specified, the script will look in '../source'.
(the script assumes it's in a 'dev' folder at the same level as 'source').

Otherwise, you can specify a source folder as the lone argument to the script.

The source folder must contain an XML file.
If there is more than 1 XML file, the first one will be used (first alphabetically).

If the source folder respresents a package, the XML file must list each file ("extension") in the package.
Each listed file must have an ID with the same name as the folder the extension is in.
The extension folder must be in the same folder as the source folder (not inside the source folder).


Output
======

The installer will be placed in the 'dev/bin' folder if it exists.
Otherwise it will be placed in your home folder.

The installer's name will be based on:
- Components: The name of the XML file (prepended "com_" if it's not already there).
- Modules:    The name of the XML file (prepended "mod_" if it's not already there).
- Packages:   The name as defined in the the XML file (converted to lowercase).
- Plugins:    The name of the XML file (prepended "plg_" if it's not already there).
- Templates:  The name as defined in the the XML file (converted to lowercase).